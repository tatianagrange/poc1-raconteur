var express = require('express');
var router = express.Router();
const path = require('path');
var mammoth = require("mammoth");
var OdtConverter = require('odt2html');
 
//multer object creation
var multer  = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
  }
})
 
var upload = multer({ 
	storage: storage, 
	fileFilter: function (req, file, cb) {
    	var filetypes = /docx|doc|odt/;
	    var mimetype = filetypes.test(file.mimetype);
	    var extname = filetypes.test(path.extname(file.originalname).toLowerCase());

	    if (mimetype && extname) {
	      return cb(null, true);
	    }

	    cb(new Error('Only doc, docx and odt files are allowed!'));
    }})
 
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
 

router.post('/', function (req, res) {
  upload.single('docxupload')(req, res, function (err) {

    if (err) {
    	res.render('index', { title: 'Express', status: "Problème d'upload : merci de n'envoyer que des fichiers doc ou docx" });
      	return
    }

    //res.render('index', { title: 'Express', status: "Upload réussi" });

    var fileName = req.file.originalname;
    var ext = fileName.substr(fileName.lastIndexOf('.') + 1);

    console.log(ext)

    if(ext == "odt"){
      OdtConverter.toHTML({
          path: "public/uploads/"+req.file.filename
      })
      .then(function(html) {
          res.render('index', { title: 'Express', doc: html });
      }, function(err) {
          console.log(err)
      })
      .done();
    }else{
      mammoth.convertToHtml({path: "public/uploads/"+req.file.filename})
      .then(function(result){
          var html = result.value; // The generated HTML
          var messages = result.messages; // Any messages, such as warnings during conversion
          res.render('index', { title: 'Express', doc: html });
      })
      .done();
    }
  })
})

module.exports = router;